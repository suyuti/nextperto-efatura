process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;
const FaturaController = require("../src/controllers/fatura.controller");
const { v4: uuidv4 } = require("uuid");

let fatura = {
    gonderen: {
        vkn     : '3810685526',
        firmaAdi: 'Gonderen Firma AS',
        adres   : 'gonderen cd.',
        ilce    : 'gungoren',
        il      : 'istanbul',
        ulke    : 'Turkiye'
    },
    alan: {
        vkn     : '3810685527',
        firmaAdi: 'Alan Firma AS',
        adres   : 'alan cd.',
        ilce    : 'Sariyer',
        il      : 'Istanbul',
        ulke    : 'Turkiye'
    },
    duzenlemeTarihi : '',
    uuid            : uuidv4(), // '44828186-d039-4f7e-b23b-792a2db5b52b',
    no              : '',
    kdv             : '180',
    araToplam       : '1000',
    vergiMatrahi    : '180',
    genelToplam     : '1180',
    kalemler: [{
        adet    : 1,
        fiyat   : 1000,
        adi     : 'Urun1'
    }
    ]

}

FaturaController.sendFatura(fatura)