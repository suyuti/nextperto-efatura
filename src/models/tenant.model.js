const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var SchemaModel = new Schema({
    key                         : String,
    adi                         : {type: String, required: true},
    marka                       : String,
    unvan                       : String,
    vkn                         : String,
    vd                          : String,
    adres                       : String,
    ilce                        : String,
    il                          : String,
    ulke                        : String,
    validFor                    : Date,
    efaturaUser                 : String,
    efaturaPwd                  : String,
    efaturaUrl                  : String,
    kontakMail                  : String,
    kontakTelefon               : String,
    faturaKodu                  : String,
    sonFaturaNo                 : String,
    sonFaturaTarihi             : String,
    gecmisTarihFaturaKodu       : String,
    sonGecmisTarihFaturaNo      : String,
    sonGecmisTarihFaturaTarihi  : String,
})

module.exports = mongoose.models.Tenant || mongoose.model("Tenant", SchemaModel);
