const soap = require("soap");

var lastLoginDate = undefined

exports.getLastLoginDate = () => {
  return lastLoginDate
}

exports.CreateClient = (url, user, password) => {
  return new Promise((resolve, reject) => {
    const wsSecurity = new soap.WSSecurity(user, password, {})
    soap.createClient(url, (err, client) => {
      if (err) {
          console.log(err)
        reject();
      } else {
        client.setSecurity(wsSecurity);
        console.log('login ok')
        lastLoginDate = new Date()
        resolve(client);
      }
    });
  });
};
