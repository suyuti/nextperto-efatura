const FaturaModel = require('../models/fatura.model')

exports.updateFatura = async (id, faturaUpdateDto) => {
    FaturaModel.findByIdAndUpdate(id, faturaUpdateDto, { new: true }, (err, doc) => {
        if (err) {
            console.log(err)
        }
        return doc
    })
}

exports.getSorgulanacakFaturalar = async () => {
    let results = await FaturaModel.find({
        $or: [
            { eFaturaDurum: 1 },
            { eFaturaDurum: -99 },
            {
                $and: [
                    { eFaturaDurum: 3 },
                    { gonderimDurumu: 2 }
                ]
            },
            {
                $and: [
                    { eFaturaDurum: 3 },
                    { gonderimDurumu: 1 }
                ]
            },
            {
                $and: [
                    { eFaturaDurum: 3 },
                    { gonderimDurumu: -1 },
                    { yanitDurumu: -1 }
                ]
            },
            {
                $and: [
                    { eFaturaDurum: 3 },
                    { gonderimDurumu: 4 },
                    { yanitDurumu: 0 }
                ]
            },
            {
                $and: [
                    { eFaturaDurum: 3 },
                    { gonderimDurumu: 3 }
                ]
            }
        ]
    }).exec()
    return results
}