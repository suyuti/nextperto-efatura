const { makeUbl } = require("../utils/ubl")
const md5 = require('md5')

exports.eFaturaKullanicisiMi = (client, vkn) => {
    return new Promise((resolve, reject) => {
        client.efaturaKullaniciBilgisi({ vergiTcKimlikNo: vkn }, (err, res) => {
            if (err) {
                reject(err)
            }
            else {
                resolve(res)
            }
        })
    })
}

exports.faturaNoUret = (client, vkn, faturaKodu) => {
    return new Promise((resolve, reject) => {
        client.faturaNoUret({ vknTckn: vkn, faturaKodu: faturaKodu }, (err, res) => {
            if (err) {
                reject(err)
            }
            else {
                resolve(res.return)
            }
        })
    })
}

exports.belgeGonder = (client, fatura) => {
    let ubl         = makeUbl(fatura)
    let ublBase64   = Buffer.from(ubl).toString('base64')
    let hash        = md5(ubl)

    console.log('UBL')
    console.log(ubl)
    return new Promise((resolve, reject) => {
        client.belgeGonder({
            vergiTcKimlikNo : fatura.gonderen.vkn,
            belgeTuru       : 'FATURA_UBL',
            belgeNo         : fatura.uuid,
            veri            : ublBase64,
            belgeHash       : hash,
            mimeType        : 'application/xml'
        }, (err, res) => {
            if (err) {
                reject(err)
            }
            else {
                resolve({res, veri: ublBase64})
            }
        })
    })
}


//---------------------------------
/*
Sayfa 9 
Tablo 3.1.4
Servis geri donus verisi

durum                   Integer                     5.2
aciklama                Karakter    En fazla 2048   Durumu “işlenemedi” olan mesajlar için hata açıklamasını içerir
gonderimDurumu          Integer                     5.4
gonderimCevabiKodu      Integer                     e-Fatura standart mesaj kodlarıdır. 1200 başarılı, diğerleri hata kodlarıdır. 
gonderimCevabiDetayi    Karakter    En fazla 2048   GİB merkezden veya alıcıdan gelen cevap detayı veya hata mesajını içerir
yanitDurumu             Integer                     5.5
yanitDetayi             Karakter    En fazla 2048   Alıcıdan gelen kabul veya red cevabı notu
yanitTarihi             Karakter    8               Alıcıdan gelen kabul veya red cevabı tarihidir.(YYYYAAGG)
olusturulmaTarihi       Karakter    17              Belgenin oluşturulma tarihidir. (YYYYAAGGSSDDssMMM)
alimTarihi              Karakter    17              Belgenin EEWS sistemine aktarım tarihidir. (YYYYAAGGSSDDssMMM)
gonderimTarihi          Karakter    17              Belgenin GİB’e gönderildiği tarihtir. (YYYYAAGGSSDDssMMM)
Ettn                    Karakter    36              e-Fatura sistemindeki Evrensel Tekil Tanımlama Numarasıdır (UUID).
belgeNo                 Karakter    16              Fatura numarası (CYS2013000000080 gibi)


5.2 Durum Kodlari:
Durum Kodu  Açıklama
1           Alındı, işlenmeyi bekliyor
2           İşlenemedi. aciklama alanında hata mesajı bulunabilir
3           İşlendi, gönderime hazır


gonderimDurmuKodu
+----+---------------------------------------------------------------+
| -2 | İptal edildi, Gönderilmeyecek                                 |
+----+---------------------------------------------------------------+
| -1 | Kuyruğa eklendi                                               |
+----+---------------------------------------------------------------+
|  0 | Gönderilemedi, sistem gönderim işlemini yeniden deneyecek.    |
+----+---------------------------------------------------------------+
|  1 | Gönderilecek                                                  |
+----+---------------------------------------------------------------+
|  2 | Gönderildi                                                    |
+----+---------------------------------------------------------------+
|  3 | GİB merkez yanıtı geldi                                       |
+----+---------------------------------------------------------------+
|  4 | Alıcı yanıtı geldi                                            |
+----+---------------------------------------------------------------+



4.5. yanıtDurumu Kodları

    Kodu    Açıklama
    -1      Yanıt gerekmiyor. Temel faturalar için yanıt beklenmez
     0      Yanıt bekleniyor. Ticari fatura için cevap bekleniyor
     1      Red cevabı geldi
     2      Kabul cevabı geldi



*/
exports.gidenBelgeSorgula = (client, gonderenVkn, belgeOid) => {
    return new Promise((resolve, reject) => {
        client.gidenBelgeDurumSorgula({
            vergiTcKimlikNo : gonderenVkn,
            belgeOid        : belgeOid,
        }, (err, res) => {
            if (err) {
                reject(err)
            }
            else {
                resolve(res)
            }
        })
    })
}

exports.onIzleme = (client, gonderenVkn, veri) => {
    return new Promise((resolve, reject) => {
        client.ublOnizleme({
            vergiTcKimlikNo : gonderenVkn,
            veri            : veri, //'FATURA_UBL',
            belgeFormati    : 'HTML' // PDF, HTML, UBL
        }, (err, res) => {
            if (err) {
                reject(err)
            }
            else {
                resolve(res)
            }
        })
    })
}

exports.cokluGidenBelgeDurumSorgula = (client, gonderenVkn, belgeOidListe) => {
    return new Promise((resolve, reject) => {
        client.cokluGidenBelgeDurumSorgula({
            vergiTcKimlikNo : gonderenVkn,
            belgeOidListe   : belgeOidListe
        }, (err, res) => {
            if (err) {
                reject(err)
            }
            else {
                resolve(res)
            }
        })
    })
}

exports.gidenBelgeleriIndir = (client, data) => {
    return new Promise((resolve, reject) => {
        client.gidenBelgeleriIndir(data, (e, res) => {
            if (e) {
                reject(e)
            }
            else {
                resolve(res.return)
            }
        })
    })
}
