const moment = require("moment");
const Client = require("../services/client.service")
const QnbService = require('../services/qnb.services');
const { makeUbl } = require("../utils/ubl");
const FaturaService = require('../services/fatura.service')
const TenantModel = require('../models/tenant.model')
const FaturaModel = require('../models/fatura.model')
const fs = require('fs')
var atob = require('atob');
const tenantModel = require("../models/tenant.model");
//const { makeUbl } = require("../utils/ubl")

const USER = process.env.ENDPOINT_USER;
const PASSWORD = process.env.ENDPOINT_PWD;
const ENDPOINT = process.env.ENDPOINT_URL;

/*
        6.1. Fatura Gönderme Adımları

            6.1.1.  İlk olarak fatura xml ya da ubl-tr formatında hazırlanır. 
                    Alıcısına belgeGonder methodu ile gonderilir. 
                    Dönüş değeri, belge başarılı bir şekilde işlenmişse EEWS sistemindeki belgenin tekil anahtarıdır.
            6.1.2.  Gönderilen belgenin durumu belirli aralıklarla 3.1.2 de bahsedilen gidenBelgeDurumSorgula methodu ile kontrol edilebilir. 
                    Giden fatura, öncelikle 5.2 de bahsedilen durum kodlarından “1-Alındı, işlenmeyi bekliyor” durumunu alır. 
                    Bu esnadan sonra belge 5.2 Durum Kodları bölümündeki durum kodlarından 2 ya da 3 no’lu durum kodlarını alabilir. 
                    Eğer 2 no’lu durum kodunu alırsa methodun dönüş parametresi olan nesnenin aciklama alanında işlenme esnasında 
                    yaşanan hataya dair bilgiler bulunur. 
                    Eğer “3” durum kodunda ise işlenme başarılıdır ve belge gönderilmeyi bekleme durumuna geçer.
            6.1.3.  Gönderilmeye çalışılan fatura daha sonra 5.4 gonderimDurumu Kodları bölümünde bahsedilen gonderim durumu kodlarından birini alır; 
                    gönderilmeyi bekleyen belge için “gönderilecek”, başarıyla gönderilen belge için “gönderildi” ve 
                    gönderme sırasında oluşan bir hata alan belge için “gönderilemedi” durum kodları verilir. 
                    Daha sonra belge GİB tarafından başarılı bir şekilde alındıysa “GİB'den merkez yanıtı geldi” ve son olarak da belge alıcısına 
                    ulaşıp da belgenin alıcısı tarafından oluşturulan uygulama yanıtı ulaşınca “Alıcı yanıtı geldi” gonderim durum kodu verilir. 
                    Alıcıdan gelen yanıtlar 5.5 de bahsedilen “2-KABUL” ya da “1-RED” yanitdurum kodlarından biri olabilir. 
                    Bu yanıt durumu alıcıdan gönderilene kadar “0-yanıt bekleniyor” durumunda kalır. Alıcı yanıtı, gönderene ulaşana kadar, 
                    gidenBelgeDurumSorgula methodu çağrılabilir. Yanıtın ulaşması ile birlikte bu methodu artık çağırmaya gerek yoktur 
                    çünkü giden faturanın durumu artık değişmeyecektir.
            6.1.4.  EEWS sistemindeki tekil anahtarlar listesini (belgeOid) giriş parametresi olarak alan gidenBelgeleriIndir methodu, 
                    başarı ile daha önceden gönderilen aynı belgeTuru ndeki (FATURA ya da UYGULAMA_YANITI) ve aynı belgeFormatı ndaki 
                    belgeleri “.zip” formatında, indirmeyi sağlar.
            6.1.5.  Yukarıda 7.1 Fatura Gönderme Adımları 1-4 arasında bahsedilen belge gönderme senaryosu belgeTuru hem FATURA hem de UYGULAMA_YANITI 
                    olan belgeler için geçerlidir. Tek fark belgeGonder methodunun belgeTuru parametresindedir. BelgeTuru kodları için 
                    5.1 Belge Türü Kodları bölümüne bakınız.


        6.2. Fatura Alma Adımları

            6.2.1.  gelenBelgeDurumSorgula methodu ile ettn’si bilinen tüm faturaların yukarıda bahsedilen 7.1 Fatura Gönderme Adımları 2 ve 3 adımlarına 
                    benzer şekilde akıbetleri sorgulanabilir. gelenBelgeDurumSorgula  servisinde bahsedilen giriş parameterleri ve dönüş nesnesini kontrol ediniz.

            6.2.2.  gelenBelgeleriAl ve gelenBelgeleriListele methodları giriş parametreleri yönünden aynıdır. Tek fark gelenBelgeleriListelede de bahsedildigi 
                    gibi amaç sadece gelen belgelerin genel bir listesini almak ise gelenBelgeleriListele , methodların dönüş değeri olan nesnenin belgeVerisi 
                    alanında “xml” verisi alabilmek için ise gelenBelgeleriAl methodu çağrılmalıdır. Her iki methodda da bulunan sonAlinanBelgeSıraNo parametresi 
                    ile kullanıcılar “artımsal” olarak, alınan belgelerin geliş sıralarına göre sorgulama yapabilmelerini sağlar. Örneğin parametre boş gönderilirse 
                    ilk method çağrımında 1-100 uncu sırada alınan belgeler geri döner, eğer parametre için 100 değeri girilerek ikinci kez bir çağırma yapılırsa bu 
                    defa da 101-200 ncu sırada gelen belgeler geri döner. Method her defasında maximum 100 kayıt döner.

            6.2.3.  gelenBelgeXmlleriniAl methoduyla kullanıcılar FATURA ya da UYGULAMA_YANITI turundeki gelen belgeleri xml string listesi olarak alabilirler.

            6.2.4.  gelenBelgeleriIndir methodu aynı yukarıda 7.1 Fatura Gönderme Adımları 4. Adımda bahsedilen method gibi “.zip” formatında ve 5.6 belgeFormati Kodları 
                    bölümünde bahsedilen farklı formatlarda gelen belgelerin toplu olarak indirilmesini sağlar.

*/

exports.sendFatura = async (fatura) => {
    try {
        console.log('Kesilecek fatura bilgisi: ')
        console.log(fatura)

        let client = await Client.CreateClient(fatura.url, fatura.user, fatura.pwd)
        let eFaturaMi = await QnbService.eFaturaKullanicisiMi(client, fatura.alan.vkn)
        // TODO eFatura kullanicisi degilse ...

        // Eger gecmis tarihli fatura ise Gecmis Tarhi fatura serisini kullan
        //let faturaNo = ''
        //if (moment(fatura.duzenlemeTarihi).isBefore(moment())) {
        //    faturaNo = await QnbService.faturaNoUret(client, fatura.gonderen.vkn, fatura.faturaKodu) // TODO FIX ME EXP kesen firmaya gore degismeli
        //}
        //else {
        //}


        let faturaNo = await QnbService.faturaNoUret(client, fatura.gonderen.vkn, fatura.faturaKodu) // TODO FIX ME EXP kesen firmaya gore degismeli
        console.log(`Alinan fatura no: ${faturaNo}`)
        let tenant = await TenantModel.findOne({ key: fatura.tenant }).exec()

        let sonAlinanFaturaNo = fatura.gecmisTarihli ? tenant.sonGecmisTarihFaturaNo : tenant.sonFaturaNo


        if (sonAlinanFaturaNo == faturaNo) {
            console.log(`${faturaNo} Bu fatura no kullaniliyor. Sonraki icin bekle`)
            return 'next'
        }



        fatura.no = faturaNo
        //fatura.duzenlemeTarihi = moment().format('YYYY-MM-DD')
        let resp = await QnbService.belgeGonder(client, fatura)
        let belgeOid = resp.res.belgeOid
        let belgeXml = resp.veri


        let _buff = atob(belgeXml)
        console.log(_buff)


        if (belgeOid) {
            await FaturaService.updateFatura(fatura._id, {
                belgeOid: belgeOid,
                faturaNo: faturaNo,
                belgeXml: belgeXml
            })

            if (fatura.gecmisTarihli) {
                await TenantModel.findOneAndUpdate(
                    { key: fatura.tenant },
                    {
                        sonGecmisTarihFaturaNo      : faturaNo,
                        sonGecmisTarihFaturaTarihi  : fatura.duzenlemeTarihi,
                    }
                ).exec()
            }
            else {
                await TenantModel.findOneAndUpdate(
                    { key: fatura.tenant },
                    {
                        sonFaturaNo     : faturaNo,
                        sonFaturaTarihi : fatura.duzenlemeTarihi,
                    }
                ).exec()
            }
    



            console.log(`Fatura gonderildi. ${belgeOid}`)
            setTimeout(() => {
                this.gidenBelgeleriSorgula(client, fatura.gonderen.vkn).then(resp => {
                    //console.log(resp)
                })
                    .catch(err => {
                        console.log(err)
                    })
            }, 5 * 60 * 1000)
        }
    }
    catch (e) {
        console.log('Error')
        console.log(e)
    }
}

// Alici EFaturaMi kontrolu yapilir
// UBL uretilir
//exports.sendFatura_1 = async (fatura) => {
//    try {
//        let client = await Client.CreateClient(ENDPOINT, USER, PASSWORD)
//        console.log(Client.getLastLoginDate())
//        let eFaturaMi   = await QnbService.eFaturaKullanicisiMi(client, fatura.gonderen.vkn)
//        let faturaNo    = await QnbService.faturaNoUret(client, fatura.gonderen.vkn, 'EXP') // TODO FIX ME EXP kesen firmaya gore degismeli
//        fatura.no = faturaNo
//        let ubl         = makeUbl(fatura)
//        console.log(ubl)
//        let ublBase64   = Buffer.from(ubl).toString('base64')
//        let pdfOnIzleme = await QnbService.onIzleme(client, fatura.gonderen.vkn, ublBase64)
//
//        await FaturaService.updateFatura(fatura._id, {aliciEFatura: true /*eFaturaMi*/, ubl: ubl, pdfOnIzleme: pdfOnIzleme.return, step: 1})
//    }
//    catch (e) {
//        console.log(e)
//    }
//}

exports.gidenBelgeleriSorgula = async (client, vkn) => {
    try {
        //let client = await Client.CreateClient(ENDPOINT, USER, PASSWORD)
        let faturalar = await FaturaService.getSorgulanacakFaturalar()
        //let liste = faturalar.map(f => f.belgeOid)
        if (faturalar.length > 0) {
            for (let fatura of faturalar) {
                //if (!fatura.ettn) {
                //    continue
                //}
                if (!fatura.belgeOid) {
                    continue
                }
                let gidenBelgePdf = ''
                let durum = await QnbService.gidenBelgeSorgula(client, vkn, fatura.belgeOid)

                if (durum.return.durum == 1) {
                    console.log(durum.return.aciklama)
                    console.log('Bekle')
                }
                else if (durum.return.durum == 2) {
                    console.log(durum.return.aciklama)
                    console.log('Hata')
                }
                else if (durum.return.durum == 3) {
                    console.log(durum.return.aciklama)
                    console.log('Basarili')

                    if (durum.return.gonderimDurumu == 2) {
                        console.log('GIBe gonderildi')
                    }
                    else if (durum.return.gonderimDurumu == 3) {
                        console.log('GIB merkez yaniti geldi')
                    }
                    else if (durum.return.gonderimDurumu == 4) {
                        console.log('GIB yanit')
                        if (durum.return.yanitDurumu == -1) {
                            console.log('Alicindan yanit beklenmiyor')

                            gidenBelgePdf = await QnbService.gidenBelgeleriIndir(client, { vergiTcKimlikNo: vkn, belgeOidListesi: [fatura.belgeOid], belgeTuru: 'FATURA', belgeFormati: 'PDF' })
                            await FaturaService.updateFatura(fatura._id, {
                                //gidenBelgeSoruSayisi: 1,
                                eFaturaDurum: durum.return.durum,
                                //eFaturaDurumAciklama: durum.return.,
                                gonderimDurumu: durum.return.gonderimDurumu,
                                gonderimCevabiKodu: durum.return.gonderimCevabiKodu,
                                gonderimCevabiDetayi: durum.return.gonderimCevabiDetayi,
                                yanitDurumu: durum.return.yanitDurumu,
                                yanitDetayi: durum.return.yanitDetayi,
                                //yanitTarihi         : durum.yanitTarihi,
                                olusturulmaTarihi: durum.return.olusturulmaTarihi,
                                //gonderimTarihi      : durum.return.,
                                ettn: durum.return.ettn,
                                //belgeNo             : durum.return.belgeNo,
                                gidenBelgePdf: gidenBelgePdf
                            })
                            // Kesilan fatura iade faturasi ise iade edilen faturayi isaretle
                            if (fatura.faturaTuru === 'IADE') {
                                await FaturaModel.findOneAndUpdate({faturaNo: fatura.iadeFaturaNo}, {
                                    iadeEdildi: true,
                                    iadeTarihi: fatura.duzenlemeTarihi
                                })
                            }
                            console.log('Fatura guncellendi')
            
                        }
                        else if (durum.return.yanitDurumu == 0) {
                            console.log('Alicidan yanit bekleniyor')
                        }
                        else if (durum.return.yanitDurumu == 1) {
                            console.log('RED')
                        }
                        else if (durum.return.yanitDurumu == 2) {
                            console.log('Kabul')
                        }
                    }
                }


                continue


                if (durum.return.yanitDurumu == -1 ||
                    durum.return.yanitDurumu == 2 ||
                    (durum.return.gonderimCevabiKodu == 1300 && durum.return.gonderimDurumu == 4)) {
                    console.log('Giden belge alinir')
                    gidenBelgePdf = await QnbService.gidenBelgeleriIndir(client, { vergiTcKimlikNo: vkn, belgeOidListesi: [fatura.belgeOid], belgeTuru: 'FATURA', belgeFormati: 'PDF' })
                }



                await FaturaService.updateFatura(fatura._id, {
                    //gidenBelgeSoruSayisi: 1,
                    eFaturaDurum: durum.return.durum,
                    //eFaturaDurumAciklama: durum.return.,
                    gonderimDurumu: durum.return.gonderimDurumu,
                    gonderimCevabiKodu: durum.return.gonderimCevabiKodu,
                    gonderimCevabiDetayi: durum.return.gonderimCevabiDetayi,
                    yanitDurumu: durum.return.yanitDurumu,
                    yanitDetayi: durum.return.yanitDetayi,
                    //yanitTarihi         : durum.yanitTarihi,
                    olusturulmaTarihi: durum.return.olusturulmaTarihi,
                    //gonderimTarihi      : durum.return.,
                    ettn: durum.return.ettn,
                    //belgeNo             : durum.return.belgeNo,
                    gidenBelgePdf: gidenBelgePdf
                })
                console.log('Fatura guncellendi')

                // Fatura basarili ise PDF'ini al
            }
            return `${faturalar.length} adet giden belge sorgulandi`
        }
    }
    catch (e) {
        console.log('Error')
        console.log(e)
    }
}

exports.tenantIcinGidenBelgeleriSorgula = async (tenantKey) => {
    let tenant = await TenantModel.findOne({ key: tenantKey }).exec()
    if (!tenant) {
        console.log(`tenant bulunamadi ${tenantKey}`)
    }
    let client = await Client.CreateClient(tenant.efaturaUrl, tenant.efaturaUser, tenant.efaturaPwd)
    await this.gidenBelgeleriSorgula(client, tenant.vkn)
    console.log(`${tenant.marka} icin giden belgeleri sorgulama tamamlandi`)
}
