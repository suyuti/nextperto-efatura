process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;
const initMongo = require("./config/mongo");
const { CreateClient } = require("./services/client.service")

const amqplib           = require("amqplib/callback_api");
const FaturaController  = require('./controllers/fatura.controller')
const AMQP_HOST         = process.env.AMQP_HOST;
const FATURA_QUEUE      = process.env.FATURA_QUEUE;

const SEC   = 1000
const MIN   = 60 * SEC
const HOUR  = 60 * MIN
const DAY   = 24 * HOUR

const USER      = process.env.ENDPOINT_USER;
const PASSWORD  = process.env.ENDPOINT_PWD;
const ENDPOINT  = process.env.ENDPOINT_URL;

let client

initMongo();




//FaturaController.sendFatura_1({})
/*
CreateClient(ENDPOINT, USER, PASSWORD).then(resp => {
    client = resp
    console.log('client login')
    setInterval(() => {
        CreateClient(ENDPOINT, USER, PASSWORD).then(resp => {
            console.log('client login')
            client = resp
        })
        .catch(e => {
            console.log('login error')
        })
    }, (25 * MIN))
})
.catch(e => {
    console.log('login error')
})
*/


amqplib.connect(AMQP_HOST, (err, connection) => {
    if (err) {
        console.error(err.stack);
        return
    }
    console.log(`connected to ${AMQP_HOST}`)
    connection.createChannel((err, channel) => {
        if (err) {
            console.log(err)
            return
        }
        channel.assertQueue(FATURA_QUEUE, { durable: true }, (err) => {
            if (err) {
                console.log(err)
            }
            console.log(`Queue ok ${FATURA_QUEUE}`)
            channel.prefetch(1);
            channel.consume(FATURA_QUEUE, (data) => {
                if (data == null) {
                    return
                }
                console.log('FATURA')
                let fatura = JSON.parse(data.content.toString())

                if (fatura.command == 'gidenBelgeleriSorgula') {
                    FaturaController.tenantIcinGidenBelgeleriSorgula(fatura.tenant)
                    return channel.ack(data)
                }
                else {
                    FaturaController.sendFatura(fatura).then(resp => {
                        if (resp == 'next') {
                            //return channel.nackAll(false)
                            return channel.nack(data,false, true)
                        }
                        console.log('OK')
                        return channel.ack(data)
                    })
                }

            })
        })
    })

})
